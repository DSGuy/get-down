extends Node

onready var music = get_node("music")

var miliSecond = 0
var second = 0
var minute = 0

var stream = ''
var volume = 1
var drain = false

func _ready():
	music.set_volume(volume)
	#madgapuff_march()
	#surfing()
	music.connect("finished", self, "_on_music_finished")
	pass

func _process(delta):
	
	clock()
	if drain:
		volume -= 0.01
		music.set_volume(volume)
		if volume <= 0:
			music.stop()
			set_process(false)
			second = 0
			miliSecond = 0
			minute = 0
			drain = false
	pass

func clock():
	miliSecond += 1
	if miliSecond >= 99:
		second += 1
		miliSecond = 0
	if second >= 59:
		minute += 1
		second = 0
	
	print(str(minute, ":", second, ".", miliSecond))
	pass

func choose_song():
	randomize()
	var rndSong = randi() % 2

	if rndSong == 0:
		madgapuff_march()
	if rndSong == 1:
		surfing()
	pass

func _on_music_finished():
	set_process(false)
	choose_song()
	pass

func madgapuff_march():
	second = 0
	miliSecond = 0
	minute = 0

	volume = 1
	music.set_volume(volume)
	stream = 'madgapuff'
	var aud = load("res://res/audio/music/madgapuff_march.ogg")
	music.set_stream(aud)
	set_process(true)
	music.play()
	pass

func surfing():
	second = 0
	miliSecond = 0
	minute = 0
	
	volume = 1
	music.set_volume(volume)
	stream = 'surfing'
	var aud = load("res://res/audio/music/surfing.ogg")
	music.set_stream(aud)
	set_process(true)
	music.play()
	pass