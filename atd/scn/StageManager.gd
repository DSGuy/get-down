# script: StageManager

extends CanvasLayer

const NORMAL = "res://src/stages/normal.tscn"

var is_changing = false
var tree
var root
var cur_scene = null

signal stage_changed

func _ready():
	set_process(true)
	pass

func _process(delta):
	tree = get_tree()
	root = tree.get_root()
	cur_scene = root.get_child(root.get_child_count() - 1 )
	pass

func change_stage(stage_path):
	if is_changing: return
	
	is_changing = true
	get_tree().get_root().set_disable_input(false)
	
	# fade to black
	get_node("anim").play("fade_in")
	#audio_player.play("sfx_swooshing")
	yield(get_node("anim"), "animation_finished")
	

	# change stage
	get_tree().change_scene(stage_path)
	emit_signal("stage_changed")
	
	# delete previous stage
	cur_scene.free()
	
	# fade from black
	get_node("anim").play("fade_out")
	yield(get_node("anim"), "animation_finished")
	
	is_changing = false
	get_tree().get_root().set_disable_input(false)
	pass

func white_change_stage(stage_path):
	if is_changing: return

	is_changing = true
	get_tree().get_root().set_disable_input(false)
	
	# fade to white
	get_node("animt").play("fade_in")
	#audio_player.play("sfx_swooshing")
	yield(get_node("animt"), "animation_finished")


	# change stage
	get_tree().change_scene(stage_path)
	emit_signal("stage_changed")
	
	# delete previous stage
	cur_scene.free()
	
	# fade from white
	get_node("animt").play("fade_out")
	yield(get_node("animt"), "animation_finished")
	
	is_changing = false
	get_tree().get_root().set_disable_input(false)
	pass