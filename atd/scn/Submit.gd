# Code written by Oladeji Sanyaolu (Submit-3), Copyright (C) 2018

extends Control


onready var gamejolt = get_node("gamejolt")

onready var userTxt = get_node("canvas/panel/textbox/usertxt")
onready var tokeTxt = get_node("canvas/panel/textbox/toketxt")
onready var login   = get_node("canvas/panel/button/login")
onready var isAuth = get_node("canvas/panel/label/auth")

var authenticated = false
var user = ''
var toke = ''

func _ready():
	hide_all()

	userTxt.set_text("")
	tokeTxt.set_text("")

	connect("draw", self, "_on_draw")
	connect("hide", self, "_on_hide")

	get_node("canvas/panel/close/one").connect("pressed", self, "_hide")
	get_node("canvas/panel/close/two").connect("pressed", self, "_hide")
	get_node("canvas/panel/close/thr").connect("pressed", self, "_hide")
	get_node("canvas/panel/close/fore").connect("pressed", self, "_hide")

	gamejolt.connect("api_authenticated", self, "_on_api_authenticated")
	login.connect("pressed", self, "_authenticate")
	pass


func _on_draw():
	get_tree().set_pause(true)

	if !authenticated:
		isAuth.set_text(str(""))
	
	userTxt.set_text(str(""))
	tokeTxt.set_text(str(""))
	pass

func _on_hide():
	get_tree().set_pause(false)
	pass

func _hide():
	hide_all()
	pass

func _on_api_authenticated(sucess):
	if sucess:
		authenticated = true

		user = userTxt.get_text()
		toke = tokeTxt.get_text()

		isAuth.set_text(str(user))
		#isAuth.font_color(set_color(Color(1, 0, 0)))
		hide()
	else:
		isAuth.set_text(str("Failed"))
		#isAuth.font_color(set_color(Color(1, 0, 0)))
	pass

func _authenticate():
	isAuth.set_text("Connecting....")
	#isAuth.font_color(set_color(Color(0, 0, 0)))
	gamejolt.auth_user(tokeTxt.get_text(), userTxt.get_text())
	pass

func show_all():
	show()
	get_node("canvas/panel").show()
	pass

func hide_all():
	hide()
	get_node("canvas/panel").hide()
	pass
