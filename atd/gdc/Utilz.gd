extends Node

var mnode setget , _get_main_node
var mouse setget , _get_mouse_pos
var vsize setget , _get_view_size
var width setget , _get_width
var height setget , _get_height

var plrHealth = 0
var score = 0
var paused = false


func _get_mouse_pos():
	return get_viewport().get_mouse_pos()

func _get_view_size():
    return get_tree().get_root().get_visible_rect().size
    pass

func _get_main_node():
	var root = get_tree().get_root()
	return root.get_child( root.get_child_count()-1 )
	pass

func _get_width():
	return ProjectSettings.get_setting("display/window/size/width")
	pass 

func _get_height():
	return ProjectSettings.get_setting("display/window/size/height")
	pass

func remote_call(src_node, method, arg0 = null, arg1 = null):
	src_node = find_node(src_node)
	
	if src_node and src_node.has_method(method):
		if arg0 and arg1:
			return src_node.call(method, arg0, arg1)
		if arg0:
			return src_node.call(method, arg0)
		
		return src_node.call(method)
	pass

