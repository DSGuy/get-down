# Code written by Oladeji Sanyaolu (AudioPlayer), Copyright (C) 2018

extends AudioStreamPlayer2D

const TEST = preload("res://res/audio/music/test_one.ogg")

func play_music(music):
	position = Vector2(GameUtilz.width/2, GameUtilz.height/2)
	stream = music
	play()
	pass