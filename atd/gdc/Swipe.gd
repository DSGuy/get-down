
extends Node

signal swipe_up
signal swipe_down

var swipe_start = null
var minimum_drag = 100


func _ready():
    set_process_unhandled_input(true)
    pass
    
func _unhandled_input(event):
    if event.is_action_pressed("ui_click"):
        swipe_start = get_viewport().get_mouse_pos()#get_global_mouse_pos()
    if event.is_action_released("ui_click"):
        _calculate_swipe(get_viewport().get_mouse_pos())#get_global_mouse_pos())
    pass
    
func _calculate_swipe(swipe_end):
    if swipe_start == null: 
        return
    var swipe = swipe_end - swipe_start
    print(swipe)
    if abs(swipe.y) > minimum_drag:
        if swipe.y > 0: # Go down
            emit_signal("swipe_down")

        else: # Go up
            emit_signal("swipe_up")

    pass