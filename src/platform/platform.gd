# Code written by Oladeji Sanyaolu (platform)
# For the Platformer GameJam September 2018

extends StaticBody2D

func _ready():
	set_physics_process(true)
	pass 

func _physics_process(delta):
	move_local_y(Global.PLATFORM_SPEED)
	
	if position.y < -20:
		queue_free()
	if Global.PLR_DEAD:
		var sx = scale.x
		var sy = scale.y
		if sx > 0 and sy > 0:
			sx -= 0.03
			sy -= 0.03
		else:
			queue_free()
		scale = Vector2(sx, sy)
	pass