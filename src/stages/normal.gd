# Code written by Oladeji Sanyaolu (stage/normal)
# For the Platformer GameJam September 2018

extends Node2D

var player = load("res://src/player/player.tscn")
var platform_base = load("res://src/platform/platform_base.tscn")
var spawner = load("res://src/spawner/spawner.tscn")
var background = load("res://src/background/background.tscn")

var plr = player.instance()
var bg = background.instance()

func _ready():
	$menu/bstscore.text = str("Bestscore: ", Save.bestscore)
	$menu/anim.connect("animation_finished", self, "_on_menu_anim_finished")
	
	$fail/anim.connect("animation_started", self, "_on_fail_anim_started")
	$fail/anim.connect("animation_finished", self, "_on_fail_anim_finished")
	
	$ui.hide()
	add_child(bg)
	set_process_input(true)
	pass

func _process(delta):
	Global.SCORE += 1
	$ui/score.text = str(Global.SCORE)
	$fail/your_score.text = str("Your Score: ", Global.SCORE)
	
	if Global.PLR_DEAD:
		$fail/anim.play("slide_up")
		$ui.hide()
		set_process(false)
	pass

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if !Global.PLR_DEAD:
			$menu/anim.play("slide_up")
			set_process_input(false)
		else:
			$fail/anim.play("slide_up_again")
			set_process_input(false)
	pass

func game_setup():
	var base = platform_base.instance()
	var spawn = spawner.instance()
	
	Global.PLR_DEAD = false

	plr.position = Vector2(Utilz.width/2, -100)
	base.position = Vector2(base.position.x, Utilz.height+50)
	
	add_child(plr)
	add_child(base)
	add_child(spawn)
	
	Global.SCORE = 0
	$ui.show()
	
	set_process(true)
	pass

func game_resetup():
	var base = platform_base.instance()
	var spawn = spawner.instance()
	
	Global.PLR_DEAD = false #plr.game_over = false
	plr.set_physics_process(true)
	plr.all_set = false
	plr.motion.y = 0
	
	plr.position = Vector2(Utilz.width/2, -100)
	base.position = Vector2(base.position.x, Utilz.height+50)
	
	add_child(base)
	add_child(spawn)
	
	Global.SCORE = 0
	$ui.show()
	
	set_process(true)
	pass

func _on_menu_anim_finished(anim_name):
	game_setup()
	$menu.hide()
	pass

func _on_fail_anim_started(anim_name):
	if anim_name == "slide_up":
		if Global.SCORE > Save.bestscore:
			Save.set_bestscore(Global.SCORE)
		$fail/failbstscore.text = str("Bestscore: ", Save.bestscore)
	pass

func _on_fail_anim_finished(anim_name):
	if anim_name == "slide_up":
		set_process_input(true)
	elif anim_name == "slide_up_again":
		game_resetup()
	pass