# Code written by Oladeji Sanyaolu (splashscreen), Copyright (C) 2018

extends Node

onready var animo = get_node("anim_one")
onready var animt = get_node("anim_two")
onready var animth = get_node("anim_thr")
onready var animf = get_node("anim_for")
onready var animfi = get_node("anim_fiv")

func _ready():
	animo.play("fade_in")
	
	animo.connect("animation_finished", self, "_on_animo_finished")
	animt.connect("animation_finished", self, "_on_animt_finished")
	animth.connect("animation_finished", self, "_on_animth_finished")
	animf.connect("animation_finished", self, "_on_animf_finished")
	animfi.connect("animation_finished", self, "_on_animfi_finished")

	set_process_unhandled_input(true)
	pass

func _on_animo_finished(anim_name):
	animt.play("dsguy")
	pass

func _on_animt_finished(anim_name):
	animth.play("fade_out")
	pass

func _on_animth_finished(anim_name):
	animf.play("godot_fade_in")
	pass

func _on_animf_finished(anim_name):
	animfi.play("godot_fade_out")
	pass

func _on_animfi_finished(anim_name):
	StageManager.white_change_stage(StageManager.NORMAL)
	pass

func _unhandled_input(event):
	if event.is_action_pressed("ui_accept") or event.is_action_pressed("switch"):
		StageManager.white_change_stage(StageManager.NORMAL)
		pass
	pass