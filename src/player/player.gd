# Code written by Oladeji Sanyaolu (player)
# For the Platformer GameJam September 2018

extends KinematicBody2D

# Speed
var acceleration = 20
var maxspeed = 120

# Jump
var jump = -400
var up = Vector2(0, -1)

# Gravity
var gravity = 20

# Motion
var motion = Vector2()
var friction = 0.4

# Are we all set?
var all_set = false

# Is the game over :(
var game_over = false

# Animation Node
onready var animspr = get_node("animspr")

# Input
#var input_up = Input.is_action_pressed("ui_up")
#var input_down = Input.is_action_pressed("ui_down")
#var input_left = Input.is_action_pressed("ui_left")
#var input_right = Input.is_action_pressed("ui_right")
#var input_jump = Input.is_action_pressed("ui_jump")

func _ready():
	add_to_group("player")
	$area.connect("area_entered", self, "_on_area_entered")
	set_physics_process(true)
	pass 

func _on_area_entered(other): # since the enemy is the only other area and other.is_in_group("enemy") doesn't work....
	# if other.is_in_group("enemy"):
	Global.PLR_DEAD = true # we can kill the player upon collision
	pass

func _physics_process(delta):
	motion()
	positioning()
	game_over()
	
	if Global.PLR_DEAD:
		$hit.play()
	pass

func motion():
	if Input.is_action_pressed("ui_right"):
		motion.x += acceleration # let thou move right
		motion.x = min(motion.x+acceleration, maxspeed)
		animspr.flip_h = false
		animspr.play("running")
		
	elif Input.is_action_pressed("ui_left"):
		motion.x -= acceleration # let thou move left 
		motion.x = max(motion.x-acceleration, -maxspeed)
		animspr.flip_h = true
		animspr.play("running")
	else:
		motion.x = lerp(motion.x, 0, friction) # okay...so the player is pressing neither so lets like totally stop
		animspr.play("idle")
	
	if is_on_floor(): # tha player is on da floor
		all_set = true
		if Input.is_action_just_pressed("ui_jump"): # tha player is try-ann ta jamp
			motion.y += jump # tha player is jumpan
			$jump.play()
	else:
		if motion.y > 0:
			animspr.play("jumping")
		else:
			animspr.play("landing")
		
	motion.y += gravity # gravity! sweet gravity!
	motion = move_and_slide(motion, up) # apply the whole physics without much effort!
	pass

func positioning():
	if position.x > Utilz.width: # The player will appear at the other side of the screen
		position.x = -scale.x
	elif position.x < -scale.x: # The player will appear at the other side of the screen
		position.x = Utilz.width
	
	if !all_set:
		if position.y > Utilz.height: # The player will appear at the other side of the screen
			position.y = -scale.y
		elif position.y < -scale.y: # The player will appear at the other side of the screen
			position.y = Utilz.height
	else:
		if position.y > Utilz.height or position.y < -scale.y-20: # Oh that's too bad!
			Global.PLR_DEAD = true#game_over = true
	pass

func game_over():
	if Global.PLR_DEAD:#game_over:
		hide()
		set_physics_process(false)
	else:
		show()
		set_physics_process(true)
	pass
