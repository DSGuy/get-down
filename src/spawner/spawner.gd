# Code written by Oladeji Sanyaolu (spawner)
# For the Platformer GameJam September 2018

extends Node2D

###################### Load the scenes #################

var platform_one = load("res://src/platform/platform_one.tscn")
var platform_two = load("res://src/platform/platform_two.tscn")
var platform_thr = load("res://src/platform/platform_thr.tscn")

##### Enemy Scene #####

var enemy = load("res://src/enemy/enemy.tscn")

###################### Load the Nodes ##################

onready var platform = get_node("platform")

func _ready():
	set_process(true)
	platform.connect("timeout", self, "_on_platform_timeout")
	platform.start()
	pass

func _process(delta):
	
	if Global.PLR_DEAD:
		queue_free()
	
	if Global.SCORE > 10000:
		platform.set_wait_time(3)
		Global.PLATFORM_SPEED = -3
	else:
		platform.set_wait_time(1.25)
		Global.PLATFORM_SPEED = -1
		
	pass

func _on_platform_timeout():
	randomize()
	
	var platnum = randi() % 3
	var enmspawn = randi() % 10
	var platpos = randi() % Utilz.width
	
	if platnum == 0:
		var platone = platform_one.instance()
		platone.position = Vector2(platpos, Utilz.height+20)
		
		if enmspawn <= 1:
			var enm = enemy.instance()
			enm.position = Vector2(platpos, platone.position.y-10)
			Utilz.mnode.add_child(enm)
			
		Utilz.mnode.add_child(platone)
		
	elif platnum == 1:
		var plattwo = platform_two.instance()
		plattwo.position = Vector2(platpos, Utilz.height+20)
		
		if enmspawn <= 1:
			var enm = enemy.instance()
			enm.position = Vector2(platpos, plattwo.position.y-10)
			Utilz.mnode.add_child(enm)
			
		Utilz.mnode.add_child(plattwo)
		
	elif platnum == 2:
		var platthr = platform_thr.instance()
		platthr.position = Vector2(platpos, Utilz.height+20)
		
		if enmspawn <= 1:
			var enm = enemy.instance()
			enm.position = Vector2(platpos, platthr.position.y-10)
			Utilz.mnode.add_child(enm)
			
		Utilz.mnode.add_child(platthr)
		 
	pass
