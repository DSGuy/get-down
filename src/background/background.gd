extends Node2D

onready var star = get_node("bg/spr")
onready var nstar = get_node("bg/spr2")

var velocity = -0.5

func _ready():
	set_process(true)
	pass

func _process(delta):
	star.move_local_y(velocity)
	nstar.move_local_y(velocity)
	
	if star.position.y < -Utilz.height:
		star.position = Vector2(star.position.x, get_viewport_rect().size.y)
	
	if nstar.position.y < -Utilz.height:
		nstar.position = Vector2(nstar.position.x, get_viewport_rect().size.y)
	pass

