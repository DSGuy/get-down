# Code written by Oladeji Sanyaolu (enemy)
# For the Platformer GameJam September 2018

extends KinematicBody2D

# Node
onready var animspr = get_node("animspr")
onready var plr = Utilz.mnode.get_node("player")

# Speed
var acceleration = 20
var maxspeed = 120

# Jump
var jump = -400
var up = Vector2(0, -1)

# Gravity
var gravity = 30

# Motion
var motion = Vector2()
var friction = 0.4

func _ready():
	add_to_group("enemy")
	set_physics_process(true)
	pass

func _physics_process(delta):
	motion()
	dead()
	pass

func motion():
	if plr.position.y >= position.y-2 and plr.position.y <= position.y+2 and plr.is_on_floor(): # The player is on my ground! Attack!!!!
		if plr.position.x > position.x: # The player is right!
			motion.x += acceleration # let thou move right
			motion.x = min(motion.x+acceleration, maxspeed)
			animspr.flip_h = false
			animspr.play("walking")
			if !$robot.is_playing():
				$robot.play()
		
		elif plr.position.x < self.position.x: # The player is left!
			motion.x -= acceleration # let thou move left 
			motion.x = max(motion.x-acceleration, -maxspeed)
			animspr.flip_h = true
			animspr.play("walking")
			if !$robot.is_playing():
				$robot.play()
		
	else:
		motion.x = lerp(motion.x, 0, friction) # okay...so the player is not on my ground so lets like totally stop
		animspr.play("idle")
	
	if is_on_floor(): # we don't need this yet....
		pass
	
	if position.y < -5 or position.y > Utilz.height+20: # die! die!
		queue_free()
		
	motion.y += gravity # gravity! sweet gravity!
	motion = move_and_slide(motion, up) # apply the whole physics without much effort!
	pass

func dead():
	if Global.PLR_DEAD:
		queue_free()
	pass
